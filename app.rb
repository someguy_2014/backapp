require 'sinatra'
require 'sinatra/activerecord'
require 'sinatra/json'
require 'json'
require 'consoleapp'
require './fibonacci.rb'

set :port, 3000
set :database, 'sqlite3:backup.sqlite3'

get '/fibonacci' do
  generate_json
end

post '/fibonacci' do
  generate_json(true)
end

def generate_json(save = false)
  fibonacci_sequence_length = params['n'].to_i
  fibonacci                 = Fibonacci.find_by(sequence_length: fibonacci_sequence_length)

  if fibonacci.blank?
    fibonacci_multiplication_table = FibonacciMultiplicationTable.new(fibonacci_sequence_length) 
    save_fibonacci_multiplication_table(fibonacci_multiplication_table) if save

    multiplication_table = fibonacci_multiplication_table.multiplication_table.to_json
  else
    multiplication_table = fibonacci.multiplication_table
  end

  json JSON.parse(multiplication_table)
end

def save_fibonacci_multiplication_table(fibonacci_multiplication_table)
   Fibonacci.create!(
      sequence_length:      fibonacci_multiplication_table.fibonacci_sequence_length,
      sequence:             fibonacci_multiplication_table.fibonacci_sequence_length.to_json,
      multiplication_table: fibonacci_multiplication_table.multiplication_table.to_json,
    )
end
