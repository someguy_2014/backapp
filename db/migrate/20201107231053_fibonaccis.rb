class Fibonaccis < ActiveRecord::Migration[4.2]
  def change
    create_table :fibonaccis do |t|
      t.integer :sequence_length
      t.text    :sequence
      t.text    :multiplication_table
      
      t.timestamps

      t.index :sequence_length
    end
  end
end